package jikra.name.interview.tm.service.impl;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;
import static jikra.name.interview.tm.common.Strings.leftPadWithZeroes;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import jikra.name.interview.tm.service.api.IndexedTransaction;
import jikra.name.interview.tm.service.api.Transaction;
import jikra.name.interview.tm.service.api.TransactionIndexer;


/**
 * Default implementation of {@link TransactionIndexer}.
 */
@Service
public class DefaultTransactionIndexer implements TransactionIndexer {

    @Override
    public List<IndexedTransaction> addFormattedIndex(final List<Transaction> transactions) {
        Assert.notNull(transactions, "transactions must not be null");
        Assert.isTrue(!transactions.isEmpty(), "transactions must have at least one record");

        final Map<String, List<Transaction>> groupedTransactions = groupByPartnerName(transactions);

        final Map<String, Map<Transaction, String>> sortedTransactions = sortTransactions(groupedTransactions);

        final Map<Transaction, String> formattedIndexes = getFormattedIndexes(transactions, sortedTransactions);

        return formattedIndexes.entrySet().stream()
                .map(this::convert)
                .collect(toList());
    }

    private Map<String, List<Transaction>> groupByPartnerName(final List<Transaction> transactions) {
        return transactions.stream()
                .collect(groupingBy(Transaction::getPartnerName));
    }

    private Map<String, Map<Transaction, String>> sortTransactions(final Map<String, List<Transaction>> groupedTransactions) {
        final Map<String, Map<Transaction, String>> result = new HashMap<>();

        for (String key : groupedTransactions.keySet()) {
            if (!result.containsKey(key)) {
                result.put(key, new LinkedHashMap<>());
            }

            final List<Transaction> sortedTransactionList = sortTransactions(groupedTransactions.get(key));

            final int sortedTransactionsListSize = sortedTransactionList.size();
            final int zeroes = String.valueOf(sortedTransactionsListSize).length();
            for (int i = 0; i < sortedTransactionsListSize; i++) {
                result.get(key).put(sortedTransactionList.get(i), leftPadWithZeroes(i + 1, zeroes));
            }
        }

        return result;
    }

    private List<Transaction> sortTransactions(final List<Transaction> transactions) {
        return transactions.stream()
                .sorted(comparing(Transaction::getDateTime))
                .collect(toList());
    }

    private Map<Transaction, String> getFormattedIndexes(
            final List<Transaction> transactions,
            final Map<String, Map<Transaction, String>> sortedTransactions) {

        final Map<Transaction, String> formattedIndexes = new LinkedHashMap<>();
        for (Transaction transaction : transactions) {
            final Map<Transaction, String> transactionStringMap = sortedTransactions.get(transaction.getPartnerName());
            formattedIndexes.put(transaction, transactionStringMap.get(transaction));
        }
        return formattedIndexes;
    }

    private IndexedTransaction convert(Map.Entry<Transaction, String> entry) {
        return new IndexedTransaction(
                entry.getKey().getName(),
                entry.getKey().getDateTime(),
                entry.getKey().getPartnerName(),
                entry.getKey().getClientPhoneNumber(),
                entry.getValue());
    }
}
