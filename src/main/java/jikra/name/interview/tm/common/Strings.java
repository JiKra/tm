package jikra.name.interview.tm.common;


import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.helpers.MessageFormatter;
import org.springframework.util.Assert;


/**
 * Utility class to format {@link String}.
 */
public final class Strings {

    private Strings() {
        throw new IllegalStateException("Strings should not be instantiated");
    }

    /**
     * Left pads {@code value} with zeros according to {@code length}.
     * <p> Both {@code value} and {@code length} must be a positive number.
     *
     * @param value value to be formatted
     * @param length how many zeros will be value left-padded
     * @return zero-left-padded value
     */
    public static String leftPadWithZeroes(final int value, final int length) {
        Assert.isTrue(value >= 0, "value must be a zero or a positive number");
        Assert.isTrue(length > 0, "length must be a positive number");

        return String.format("%0" + length + "d", value);
    }

    /**
     * Formats {@code message}, replaces curly brackets with {@code values}.
     *
     * @param message message to be formatted
     * @param values values to be formatted with
     * @return formatted value
     */
    public static String fm(final String message, final Object... values) {
        if (StringUtils.isBlank(message) || ArrayUtils.isEmpty(values)) {
            return message;
        }

        return MessageFormatter.arrayFormat(message, values).getMessage();
    }
}
