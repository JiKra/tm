package jikra.name.interview.tm.service.impl;


import static java.util.stream.Collectors.toList;
import static jikra.name.interview.tm.common.Strings.fm;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import jikra.name.interview.tm.service.api.Transaction;
import jikra.name.interview.tm.service.api.TransactionParser;
import jikra.name.interview.tm.service.api.TransactionReader;


/**
 * Default implementation of {@link TransactionReader}.
 */
@Service
public class DefaultTransactionReader implements TransactionReader {

    private final TransactionParser transactionParser;

    private int maxLinesPerLine;

    @Autowired
    public DefaultTransactionReader(
            final TransactionParser transactionParser,
            @Value("${file.max.lines:100}") final int maxLinesPerLine) {

        this.transactionParser = transactionParser;
        this.maxLinesPerLine = maxLinesPerLine;
    }

    public final List<Transaction> readTransactions(final File file) {
        Assert.notNull(file, "file must not be null");

        return readLines(file).stream()
                .map(transactionParser::parse)
                .collect(toList());
    }

    private List<String> readLines(final File file) {
        try {
            final List<String> lines = FileUtils.readLines(file, "UTF-8");

            Assert.isTrue(lines.size() <= maxLinesPerLine,
                    fm("Too many lines in file {} ({}). Maximum allowed lines per file is {}",
                            file.getName(),
                            lines.size(),
                            maxLinesPerLine));

            return lines;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
