package jikra.name.interview.tm.service.impl;

import static jikra.name.interview.tm.common.Strings.fm;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.google.common.base.Splitter;
import jikra.name.interview.tm.service.api.Transaction;
import jikra.name.interview.tm.service.api.TransactionParser;


/**
 * Default implementation of {@link TransactionParser}.
 */
@Component
public class DefaultTransactionParser implements TransactionParser {

    private static final char TRANSACTION_PRIMARY_SPLITTER = ',';

    private static final char TRANSACTION_SECONDARY_SPLITTER = '/';

    public static final String SPACES_PATTERN = "\\s+";

    public static final String REPLACEABLE_COMMA = "%%%%REPLACEABLE_COMMA%%%%";

    @Override
    public Transaction parse(final String line) {
        Assert.hasText(line, "line must have text");

        final String normalizedLine = normalize(line);

        final List<String> primarySplit = Splitter.on(TRANSACTION_PRIMARY_SPLITTER).splitToList(normalizedLine);

        Assert.isTrue(primarySplit.size() == 3, fm("Wrongly formatted line: {}", normalizedLine));

        final List<String> secondarySplit = Splitter.on(TRANSACTION_SECONDARY_SPLITTER).splitToList(primarySplit.get(1));

        Assert.isTrue(secondarySplit.size() == 2, fm("Wrongly formatted line: {}", normalizedLine));

        final String transactionName = primarySplit.get(0).trim().replaceAll(REPLACEABLE_COMMA, ",");
        final String transactionDateTimeValue = primarySplit.get(2).trim();

        final String transactionPartnerName = secondarySplit.get(0).trim().replaceAll(REPLACEABLE_COMMA, ",");
        final String transactionPhoneNrValue = secondarySplit.get(1).trim();

        final Long transactionPhoneNr = parsePhone(transactionPhoneNrValue, normalizedLine);
        final LocalDateTime transactionDateTime = parseDateTime(transactionDateTimeValue, normalizedLine);

        return new Transaction(
                fulltrim(transactionName, " "),
                transactionDateTime,
                fulltrim(transactionPartnerName, " "),
                transactionPhoneNr);
    }

    private String normalize(final String value) {
        boolean isQuotation = false;

        final StringBuilder buffer = new StringBuilder();

        final String text = value.replaceAll(REPLACEABLE_COMMA, ",");

        for (int i = 0; i < text.length(); i++) {
            char c = text.charAt(i);

            if ((int) c == 34) {
                isQuotation = !isQuotation;
            } else if ((int) c == 44 && isQuotation) {
                buffer.append(REPLACEABLE_COMMA);
            } else {
                buffer.append(c);
            }
        }

        return buffer.toString();
    }

    private Long parsePhone(final String transactionPhoneNrValue, final String line) {
        final String trimmedValue = fulltrim(transactionPhoneNrValue, "");

        try {
            return Long.valueOf(trimmedValue);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException(fm("Wrongly formatted line, incorrect phone: {}", line));
        }
    }

    private LocalDateTime parseDateTime(final String transactionDateTimeValue, final String line) {
        final String trimmedValue = fulltrim(transactionDateTimeValue, " ");

        try {
            final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            final LocalDateTime dateTime = LocalDateTime.parse(trimmedValue, formatter);

            final int year = dateTime.getYear();
            Assert.isTrue(year >= 2000 && year <= 2020, fm("Wrongly formatted line, Year of date time must be between 2000-2020: {}", line));

            return dateTime;
        } catch (DateTimeParseException e) {
            throw new IllegalArgumentException(fm("Wrongly formatted line, incorrect date-time: {}", line));
        }
    }

    private String fulltrim(final String value, final String replace) {
        return value.replaceAll(SPACES_PATTERN, replace).trim();
    }
}
