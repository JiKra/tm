package jikra.name.interview.tm.service.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.junit.jupiter.api.Test;

import jikra.name.interview.tm.service.api.Transaction;


/**
 * Test for {@link DefaultTransactionParser}.
 */
class DefaultTransactionParserTest {

    final DefaultTransactionParser tested = new DefaultTransactionParser();

    @Test
    void givenNullValue_expectsIllegalArgumentException() {
        assertThatThrownBy(() -> tested.parse(null))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("line must have text");
    }

    @Test
    void givenEmptyValue_expectsIllegalArgumentException() {
        assertThatThrownBy(() -> tested.parse(""))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("line must have text");
    }

    @Test
    void givenValue_withTooFewNumberOfParts_expectsIllegalArgumentException() {
        assertThatThrownBy(() -> tested.parse("aaa,bbb"))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Wrongly formatted line: aaa,bbb");
    }

    @Test
    void givenValue_withTooManyNumberOfParts_expectsIllegalArgumentException() {
        assertThatThrownBy(() -> tested.parse("aaa,bbb,ccc,ddd"))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Wrongly formatted line: aaa,bbb,ccc,ddd");
    }

    @Test
    void givenValue_withTooFewNumberOfSubParts_expectsIllegalArgumentException() {
        assertThatThrownBy(() -> tested.parse("aaa,bbb,ccc"))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Wrongly formatted line: aaa,bbb,ccc");
    }

    @Test
    void givenValue_withTooManyNumberOfSubParts_expectsIllegalArgumentException() {
        assertThatThrownBy(() -> tested.parse("aaa,bbb/ccc/ddd,eee"))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Wrongly formatted line: aaa,bbb/ccc/ddd,eee");
    }

    @Test
    void givenValue_withWrongPhoneNumber_isNan_expectsIllegalArgumentException() {
        assertThatThrownBy(() -> tested.parse("aaa,bbb/ccc,eee"))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Wrongly formatted line, incorrect phone: aaa,bbb/ccc,eee");
    }

    @Test
    void givenValue_withWrongDateTime_expectsIllegalArgumentException() {
        assertThatThrownBy(() -> tested.parse("aaa,bbb/1234567890,eee"))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Wrongly formatted line, incorrect date-time: aaa,bbb/1234567890,eee");
    }

    @Test
    void givenValue_withWrongDateTimeFormat_withoutSeconds_expectsIllegalArgumentException() {
        assertThatThrownBy(() -> tested.parse("aaa,Partner1/1234567890,2015-09-01 12:00"))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Wrongly formatted line, incorrect date-time: aaa,Partner1/1234567890,2015-09-01 12:00");
    }

    @Test
    void givenValue_withWrongDateTimeFormat_wrongYear_1999_expectsIllegalArgumentException() {
        assertThatThrownBy(() -> tested.parse("aaa,Partner1/1234567890,1999-09-01 12:00:00"))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Wrongly formatted line, Year of date time must be between 2000-2020: aaa,Partner1/1234567890,1999-09-01 12:00:00");
    }

    @Test
    void givenValue_withWrongDateTimeFormat_wrongYear_2021_expectsIllegalArgumentException() {
        assertThatThrownBy(() -> tested.parse("aaa,Partner1/1234567890,2021-09-01 12:00:00"))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Wrongly formatted line, Year of date time must be between 2000-2020: aaa,Partner1/1234567890,2021-09-01 12:00:00");
    }

    @Test
    void givenValue_withCorrectValue_returnsTransaction() {
        final Transaction result = tested.parse("TransactionName1,Partner1/1234567890,2020-09-01 12:00:00");

        final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        final LocalDateTime dateTime = LocalDateTime.parse("2020-09-01 12:00:00", formatter);

        assertThat(result)
                .isEqualTo(new Transaction("TransactionName1", dateTime, "Partner1", 1234567890L));
    }

    @Test
    void givenValue_withCorrectValue_wherePhoneContainsSpaces_returnsTransaction() {
        final Transaction result = tested.parse("TransactionName1,Partner1/ 12   345  67890    ,2020-09-01 12:00:00");

        final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        final LocalDateTime dateTime = LocalDateTime.parse("2020-09-01 12:00:00", formatter);

        assertThat(result)
                .isEqualTo(new Transaction("TransactionName1", dateTime, "Partner1", 1234567890L));
    }

    @Test
    void givenValue_withCorrectValue_whereDateTimeContainsSpaces_returnsTransaction() {
        final Transaction result = tested.parse("TransactionName1,Partner1/1234567890,2020-09-01             12:00:00");

        final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        final LocalDateTime dateTime = LocalDateTime.parse("2020-09-01 12:00:00", formatter);

        assertThat(result)
                .isEqualTo(new Transaction("TransactionName1", dateTime, "Partner1", 1234567890L));
    }

    @Test
    void givenValue_withLotsOfSpaces_returnsTransaction() {
        final Transaction result = tested
                .parse("Transaction        Name1     ,   Partner     1 /     1      2345678 90 ,2020-09-01                     12:00:00");

        final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        final LocalDateTime dateTime = LocalDateTime.parse("2020-09-01 12:00:00", formatter);

        assertThat(result)
                .isEqualTo(new Transaction("Transaction Name1", dateTime, "Partner 1", 1234567890L));
    }

    @Test
    void givenValue_withLotsOfSpaces_andQuotations_returnsTransaction() {
        final Transaction result = tested
                .parse(" \"Transaction      Name1\"   ,  \"Partner     1\" /     1      2345678 90 ,2020-09-01                     12:00:00");

        final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        final LocalDateTime dateTime = LocalDateTime.parse("2020-09-01 12:00:00", formatter);

        assertThat(result)
                .isEqualTo(new Transaction("Transaction Name1", dateTime, "Partner 1", 1234567890L));
    }

    @Test
    void givenValue_withLotsOfSpaces_andQuotations_withCommas_returnsTransaction() {
        final Transaction result = tested
                .parse(" \"Transaction  ,    Name,1\"   ,  \"Partner  ,   1\" /     1      2345678 90 ,2020-09-01                    12:00:00");

        final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        final LocalDateTime dateTime = LocalDateTime.parse("2020-09-01 12:00:00", formatter);

        assertThat(result)
                .isEqualTo(new Transaction("Transaction , Name,1", dateTime, "Partner , 1", 1234567890L));
    }

    @Test
    void givenValue_withVariable_returnsTransaction() {
        final Transaction result = tested
                .parse(" \"Transaction  %%%%REPLACEABLE_COMMA%%%%    Name,1\"   ,  \"Partner  ,   1\" /  1  2345678 90 ,2020-09-01  12:00:00");

        final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        final LocalDateTime dateTime = LocalDateTime.parse("2020-09-01 12:00:00", formatter);

        assertThat(result)
                .isEqualTo(new Transaction("Transaction , Name,1", dateTime, "Partner , 1", 1234567890L));
    }
}
