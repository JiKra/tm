package jikra.name.interview.tm.service.api;

import java.util.List;


/**
 * Service to format transactions.
 */
public interface TransactionFormatter<T extends Transaction> {

    /**
     * Formats list of transactions.
     *
     * @param transactions transactions to be formatted
     * @return formatted value
     */
    String format(List<T> transactions);

}
