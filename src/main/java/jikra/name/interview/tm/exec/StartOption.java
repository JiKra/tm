package jikra.name.interview.tm.exec;

import java.util.Arrays;


/**
 * CLI start option wrapper.
 */
public enum StartOption {

    /**
     * Option to set files to be parsed.
     */
    FILE("file", MANS.FILE),

    /**
     * Option to list help options.
     */
    HELP("help", MANS.HELP);

    private String option;

    private String man;

    /**
     * All-args constructor.
     *
     * @param option a CLI start option value
     * @param man a manual help
     */
    StartOption(final String option, final String man) {
        this.option = option;
        this.man = man;
    }

    public String getOption() {
        return option;
    }

    public String getMan() {
        return man;
    }

    /**
     * Returns {@link StartOption} from a string value.
     *
     * @param value a value of the option
     * @return start option
     * @throws IllegalArgumentException if no entry was found by the given value
     */
    public static StartOption from(final String value) {
        return Arrays.stream(values())
                .filter(v -> v.option.equals(value))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(value));
    }

    private static class MANS {
        static final String FILE = "--file=FILE_NAME\n\tSet of files to be parsed. "
                + "Parser will look for these files, parses their content and add transform them to the formatted transactions.\n";

        static final String HELP = "--help\n\tThis option.";
    }
}
