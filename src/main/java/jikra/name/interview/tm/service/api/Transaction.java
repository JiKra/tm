package jikra.name.interview.tm.service.api;

import java.time.LocalDateTime;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;


/**
 * Single transaction.
 */
public class Transaction {

    private final String name;

    private final LocalDateTime dateTime;

    private final String partnerName;

    private final Long clientPhoneNumber;

    /**
     * All-args constructor.
     *
     * @param name transaction name
     * @param dateTime date and time of the transaction
     * @param partnerName name of the transaction partner
     * @param clientPhoneNumber client phone number
     */
    public Transaction(final String name, final LocalDateTime dateTime, final String partnerName, final Long clientPhoneNumber) {
        this.name = name;
        this.dateTime = dateTime;
        this.partnerName = partnerName;
        this.clientPhoneNumber = clientPhoneNumber;
    }

    public String getName() {
        return name;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public String getPartnerName() {
        return partnerName;
    }

    public Long getClientPhoneNumber() {
        return clientPhoneNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final Transaction that = (Transaction) o;

        return new EqualsBuilder()
                .append(name, that.name)
                .append(dateTime, that.dateTime)
                .append(partnerName, that.partnerName)
                .append(clientPhoneNumber, that.clientPhoneNumber)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(name)
                .append(dateTime)
                .append(partnerName)
                .append(clientPhoneNumber)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                .append("name", name)
                .append("dateTime", dateTime)
                .append("partnerName", partnerName)
                .append("phoneNumber", clientPhoneNumber)
                .toString();
    }
}
