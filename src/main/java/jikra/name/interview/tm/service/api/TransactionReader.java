package jikra.name.interview.tm.service.api;

import java.io.File;
import java.util.List;


/**
 * Service to read files and parse transactions.
 */
public interface TransactionReader {

    /**
     * Reads file, validates records and returns list of transactions accordingly.
     *
     * @param file file with transactions
     * @return list of transactions
     */
    List<Transaction> readTransactions(File file);
}
