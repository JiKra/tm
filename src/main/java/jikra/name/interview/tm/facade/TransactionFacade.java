package jikra.name.interview.tm.facade;


import static java.util.stream.Collectors.toList;

import java.io.File;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import jikra.name.interview.tm.service.api.IndexedTransaction;
import jikra.name.interview.tm.service.api.TransactionFormatter;
import jikra.name.interview.tm.service.api.TransactionIndexer;
import jikra.name.interview.tm.service.api.TransactionReader;


/**
 * Facade for {@link jikra.name.interview.tm.service.api.Transaction}.
 */
@Component
public class TransactionFacade {

    private final TransactionIndexer transactionIndexer;

    private final TransactionFormatter<IndexedTransaction> transactionFormatter;

    private final TransactionReader transactionReader;

    /**
     * All-args constructor.
     *
     * @param transactionIndexer creates formatted indexes
     * @param transactionFormatter formats output
     * @param transactionReader transaction reader
     */
    @Autowired
    public TransactionFacade(
            final TransactionIndexer transactionIndexer,
            final TransactionFormatter<IndexedTransaction> transactionFormatter,
            final TransactionReader transactionReader) {

        this.transactionFormatter = transactionFormatter;
        this.transactionIndexer = transactionIndexer;
        this.transactionReader = transactionReader;
    }

    public List<String> processTransactionsFromFiles(final List<String> filePaths) {
        final List<File> files = filePaths.stream()
                .map(File::new)
                .filter(File::isFile)
                .collect(toList());

        Assert.isTrue(files.size() == filePaths.size(), "all entries must be regular accessible files");

        return files.stream()
                .map(transactionReader::readTransactions)
                .map(transactionIndexer::addFormattedIndex)
                .map(transactionFormatter::format)
                .collect(toList());
    }
}
