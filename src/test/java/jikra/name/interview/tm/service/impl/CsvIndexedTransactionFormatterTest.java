package jikra.name.interview.tm.service.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import jikra.name.interview.tm.service.api.IndexedTransaction;
import jikra.name.interview.tm.service.api.TransactionFormatter;


/**
 * Test for {@link CsvIndexedTransactionFormatter}.
 */
@SpringBootTest(classes = {CsvIndexedTransactionFormatter.class})
@DisplayName("CsvTransactionFormatter testing")
class CsvIndexedTransactionFormatterTest {

    private static final String EXPECTED_RESULT = "Partner1|01|Name1\nPartner2|02|Name2\nPartner3|03|Name3";

    @Autowired
    private TransactionFormatter<IndexedTransaction> tested;

    @Test
    @DisplayName("that service is exactly instance of CsvIndexedTransactionFormatter")
    void assertInstance_isCsvIndexedTransactionFormatter() {
        assertThat(tested)
                .isInstanceOf(CsvIndexedTransactionFormatter.class);
    }

    @Test
    @DisplayName("null value parameter throws IllegalArgumentException")
    void givenNulParameter_expectsIllegalArgumentException() {
        assertThatThrownBy(() -> tested.format(null))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("transactions must not be null");
    }

    @Test
    @DisplayName("empty list parameter throws IllegalArgumentException")
    void givenEmptyListParameter_expectsIllegalArgumentException() {
        assertThatThrownBy(() -> tested.format(new ArrayList<>()))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("transactions must have at least one record");
    }

    @Test
    @DisplayName("incorrect data with missing transaction name throws IllegalArgumentException")
    void givenIncorrectData_withMissingTransactionName_expectsIllegalArgumentException() {
        assertThatThrownBy(() -> tested.format(createData(null, "02", "Partner2")))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageStartingWith("missing name in transaction=");
    }

    @Test
    @DisplayName("incorrect data with empty transaction name throws IllegalArgumentException")
    void givenIncorrectData_withEmptyTransactionName_expectsIllegalArgumentException() {
        assertThatThrownBy(() -> tested.format(createData("", "02", "Partner2")))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageStartingWith("missing name in transaction=");
    }

    @Test
    @DisplayName("incorrect data with missing formatted index throws IllegalArgumentException")
    void givenIncorrectData_withMissingFormattedIndex_expectsIllegalArgumentException() {
        assertThatThrownBy(() -> tested.format(createData("Name2", null, "Partner2")))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageStartingWith("missing formattedIndex in transaction=");
    }

    @Test
    @DisplayName("incorrect data with empty formatted index throws IllegalArgumentException")
    void givenIncorrectData_withEmptyFormattedIndex_expectsIllegalArgumentException() {
        assertThatThrownBy(() -> tested.format(createData("Name2", "", "Partner2")))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageStartingWith("missing formattedIndex in transaction=");
    }

    @Test
    @DisplayName("incorrect data with missing partner name throws IllegalArgumentException")
    void givenIncorrectData_withMissingPartnerName_expectsIllegalArgumentException() {
        assertThatThrownBy(() -> tested.format(createData("Name2", "02", null)))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageStartingWith("missing partnerName in transaction=");
    }

    @Test
    @DisplayName("incorrect data with empty partner name throws IllegalArgumentException")
    void givenIncorrectData_withEmptyPartnerName_expectsIllegalArgumentException() {
        assertThatThrownBy(() -> tested.format(createData("Name2", "02", "")))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageStartingWith("missing partnerName in transaction=");
    }

    @Test
    @DisplayName("correct data to return valid formatted result")
    void givenCorrectData_returnsValidResult() {
        final String result = tested.format(createData("Name2", "02", "Partner2"));

        assertThat(result)
                .isEqualTo(EXPECTED_RESULT);
    }

    private List<IndexedTransaction> createData(final String name2, final String formattedIndex2, final String partnerName2) {
        final List<IndexedTransaction> transactions = new LinkedList<>();
        transactions.add(new IndexedTransaction("Name1", LocalDateTime.now(), "Partner1", 111L, "01"));
        transactions.add(new IndexedTransaction(name2, LocalDateTime.now(), partnerName2, 222L, formattedIndex2));
        transactions.add(new IndexedTransaction("Name3", LocalDateTime.now(), "Partner3", 333L, "03"));
        return transactions;
    }
}