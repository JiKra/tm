package jikra.name.interview.tm.service.impl;

import static java.util.stream.Collectors.joining;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import jikra.name.interview.tm.service.api.IndexedTransaction;
import jikra.name.interview.tm.service.api.TransactionFormatter;


/**
 * CSV implementation of {@link TransactionFormatter} for {@link IndexedTransaction}.
 */
@Service
public class CsvIndexedTransactionFormatter implements TransactionFormatter<IndexedTransaction> {

    private static final String VALUE_SEPARATOR = "|";

    private static final String LINE_SEPARATOR = "\n";

    @Override
    public String format(final List<IndexedTransaction> transactions) {
        Assert.notNull(transactions, "transactions must not be null");
        Assert.isTrue(!transactions.isEmpty(), "transactions must have at least one record");

        return transactions.stream()
                .map(this::format)
                .collect(joining(LINE_SEPARATOR));
    }

    private String format(final IndexedTransaction transaction) {
        Assert.hasText(transaction.getPartnerName(), "missing partnerName in transaction=" + transaction.toString());
        Assert.hasText(transaction.getFormattedIndex(), "missing formattedIndex in transaction=" + transaction.toString());
        Assert.hasText(transaction.getName(), "missing name in transaction=" + transaction.toString());

        return String.format("%s%s%s%s%s",
                transaction.getPartnerName(),
                VALUE_SEPARATOR,
                transaction.getFormattedIndex(),
                VALUE_SEPARATOR,
                transaction.getName());
    }
}
