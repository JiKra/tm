package jikra.name.interview.tm.service.api;


import java.time.LocalDateTime;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;


/**
 * Single transaction with formatted index.
 * <p>Index is literally order by date and time of the transaction creation formatted using leading zeros. The latter the transaction is, the
 * higher the index is.
 */
public class IndexedTransaction extends Transaction {

    private final String formattedIndex;

    /**
     * All-args constructor.
     *
     * @param name transaction name
     * @param dateTime date and time of the transaction
     * @param partnerName name of the transaction partner
     * @param clientPhoneNumber client phone number
     * @param formattedIndex index of the transaction formatted with leading zero
     */
    public IndexedTransaction(
            final String name,
            final LocalDateTime dateTime,
            final String partnerName,
            final Long clientPhoneNumber,
            final String formattedIndex) {

        super(name, dateTime, partnerName, clientPhoneNumber);
        this.formattedIndex = formattedIndex;
    }

    public String getFormattedIndex() {
        return formattedIndex;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final IndexedTransaction that = (IndexedTransaction) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(formattedIndex, that.formattedIndex)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(formattedIndex)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                .appendSuper(super.toString())
                .append("formattedIndex", formattedIndex)
                .toString();
    }
}
