package jikra.name.interview.tm.exec;


import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.stereotype.Component;


/**
 * Handler to handle CLI start options.
 */
@Component
public class StartOptionsHandler {

    private final Map<StartOption, List<String>> options = new HashMap<>();

    /**
     * Adds one option to a list.
     *
     * @param startOption a CLI start option to be added
     * @param startOptionValues a list of values of the CLI start option
     */
    public void add(final StartOption startOption, final List<String> startOptionValues) {
        options.put(startOption, startOptionValues);
    }

    /**
     * Returns {@code true} if CLI start option was help.
     *
     * @return {@code true} if CLI start option was help.
     */
    public boolean isHelp() {
        return options.containsKey(StartOption.HELP);
    }

    /**
     * Prints help to console.
     */
    public void printHelp() {
        System.out.println("Following options are available:\n");
        Arrays.stream(StartOption.values())
                .map(StartOption::getMan)
                .forEach(System.out::println);
    }

    /**
     * Returns input directory name by the start options.
     *
     * @return input directory name
     */
    public Set<String> getFiles() {
        return hasOption(StartOption.FILE)
                ? new HashSet<>(options.get(StartOption.FILE))
                : new HashSet<>();
    }

    private boolean hasOption(StartOption startOption) {
        return options.containsKey(startOption)
                && !options.get(startOption).isEmpty();
    }
}
