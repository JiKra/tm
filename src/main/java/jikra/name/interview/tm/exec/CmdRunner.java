package jikra.name.interview.tm.exec;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import jikra.name.interview.tm.Solution;


/**
 * Command line runner. Reads start options and prints result.
 */
@Component
public class CmdRunner implements ApplicationRunner {

    private static final Logger logger = LoggerFactory.getLogger(CmdRunner.class);

    private final StartOptionsHandler startOptionsHandler;

    private final Solution solution;

    /**
     * All-args constructor.
     *
     * @param startOptionsHandler start options handler
     * @param solution solution class
     */
    @Autowired
    public CmdRunner(final StartOptionsHandler startOptionsHandler, final Solution solution) {
        this.startOptionsHandler = startOptionsHandler;
        this.solution = solution;
    }

    @Override
    public void run(final ApplicationArguments args) {

        // Load all CLI options
        args.getOptionNames().stream()
                .map(StartOption::from)
                .forEach(s -> startOptionsHandler.add(s, args.getOptionValues(s.getOption())));

        // Handle potential --help option and quit application if present or no options are present at all
        if (startOptionsHandler.isHelp()) {
            startOptionsHandler.printHelp();
            return;
        }

        final Set<String> files = startOptionsHandler.getFiles();

        if (files.isEmpty()) {
            startOptionsHandler.printHelp();
        } else {
            printFiles(files);
        }
    }

    private void printFiles(final Set<String> files) {
        try {
            files.stream()
                    .map(solution::solution)
                    .forEach(System.out::println);
        } catch (Exception e) {
            logger.error("Error while running application", e);
            System.out.println("Error while running application: " + e.getMessage());
        }
    }
}
