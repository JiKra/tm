package jikra.name.interview.tm;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.ImmutableList;
import jikra.name.interview.tm.facade.TransactionFacade;


@Component
public class Solution {

    private final TransactionFacade transactionFacade;

    @Autowired
    public Solution(final TransactionFacade transactionFacade) {
        this.transactionFacade = transactionFacade;
    }

    public String solution(final String location) {
        return transactionFacade.processTransactionsFromFiles(ImmutableList.of(location))
                .stream()
                .reduce("", String::concat);
    }
}
