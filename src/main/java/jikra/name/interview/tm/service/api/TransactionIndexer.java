package jikra.name.interview.tm.service.api;


import java.util.List;


/**
 * Adds index to transaction.
 * <p>Index is literally order by date and time of the transaction creation formatted using leading zeros. The latter the transaction is, the
 * higher the index is.
 */
public interface TransactionIndexer {

    /**
     * Returns list of {@link IndexedTransaction} with formatted index.
     *
     * @param transactions transactions
     * @return indexed transactions
     */
    List<IndexedTransaction> addFormattedIndex(List<Transaction> transactions);

}
