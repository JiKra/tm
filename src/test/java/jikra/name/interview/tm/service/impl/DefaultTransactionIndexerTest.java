package jikra.name.interview.tm.service.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import jikra.name.interview.tm.service.api.IndexedTransaction;
import jikra.name.interview.tm.service.api.Transaction;


/**
 * Test for {@link DefaultTransactionIndexer}.
 */
@DisplayName("DefaultTransactionIndexer testing")
class DefaultTransactionIndexerTest {

    private final LocalDateTime now = LocalDateTime.now();

    private DefaultTransactionIndexer tested = new DefaultTransactionIndexer();

    @Test
    @DisplayName("null value parameter throws IllegalArgumentException")
    void givenNulParameter_expectsIllegalArgumentException() {
        assertThatThrownBy(() -> tested.addFormattedIndex(null))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("transactions must not be null");
    }

    @Test
    @DisplayName("empty list parameter throws IllegalArgumentException")
    void givenEmptyListParameter_expectsIllegalArgumentException() {
        assertThatThrownBy(() -> tested.addFormattedIndex(new ArrayList<>()))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("transactions must have at least one record");
    }

    @Test
    @DisplayName("correct list of transactions returns the same list enriched with formatted indexes by partner and date-time")
    void givenTransactionList_withThreePartners_returnsListWithFormattedIndexes() {
        final List<IndexedTransaction> indexedTransactions = tested.addFormattedIndex(createTransactions());

        assertThat(indexedTransactions)
                .isEqualTo(createIndexedTransactions());
    }

    private List<Transaction> createTransactions() {
        final List<Transaction> transactions = new LinkedList<>();
        transactions.add(new Transaction("Transaction1", now.plusDays(5), "Partner1", 111L));
        transactions.add(new Transaction("Transaction6", now.plusDays(10), "Partner2", 111L));
        transactions.add(new Transaction("Transaction22", now.plusDays(66), "Partner2", 111L));
        transactions.add(new Transaction("Transaction8", now.plusDays(3), "Partner2", 111L));
        transactions.add(new Transaction("Transaction3", now.plusDays(1), "Partner1", 111L));
        transactions.add(new Transaction("Transaction14", now.plusDays(25), "Partner2", 111L));
        transactions.add(new Transaction("Transaction11", now.plusDays(1), "Partner3", 111L));
        transactions.add(new Transaction("Transaction2", now.plusDays(10), "Partner1", 111L));
        transactions.add(new Transaction("Transaction9", now.plusDays(5), "Partner3", 111L));
        transactions.add(new Transaction("Transaction7", now.plusDays(1), "Partner2", 111L));
        transactions.add(new Transaction("Transaction10", now.plusDays(10), "Partner3", 111L));
        transactions.add(new Transaction("Transaction18", now.plusDays(99), "Partner2", 111L));
        transactions.add(new Transaction("Transaction19", now.plusDays(90), "Partner2", 111L));
        transactions.add(new Transaction("Transaction4", now.plusDays(3), "Partner1", 111L));
        transactions.add(new Transaction("Transaction12", now.plusDays(3), "Partner3", 111L));
        transactions.add(new Transaction("Transaction5", now.plusDays(5), "Partner2", 111L));
        transactions.add(new Transaction("Transaction13", now.plusDays(55), "Partner2", 111L));
        transactions.add(new Transaction("Transaction15", now.plusDays(35), "Partner2", 111L));
        transactions.add(new Transaction("Transaction16", now.plusDays(45), "Partner2", 111L));
        transactions.add(new Transaction("Transaction17", now.plusDays(76), "Partner2", 111L));
        transactions.add(new Transaction("Transaction23", now.plusDays(88), "Partner2", 111L));
        transactions.add(new Transaction("Transaction20", now.plusDays(293), "Partner2", 111L));
        transactions.add(new Transaction("Transaction21", now.plusDays(34), "Partner2", 111L));
        return transactions;
    }

    private List<IndexedTransaction> createIndexedTransactions() {
        final List<IndexedTransaction> transactions = new LinkedList<>();
        transactions.add(new IndexedTransaction("Transaction1", now.plusDays(5), "Partner1", 111L, "3"));
        transactions.add(new IndexedTransaction("Transaction6", now.plusDays(10), "Partner2", 111L, "04"));
        transactions.add(new IndexedTransaction("Transaction22", now.plusDays(66), "Partner2", 111L, "10"));
        transactions.add(new IndexedTransaction("Transaction8", now.plusDays(3), "Partner2", 111L, "02"));
        transactions.add(new IndexedTransaction("Transaction3", now.plusDays(1), "Partner1", 111L, "1"));
        transactions.add(new IndexedTransaction("Transaction14", now.plusDays(25), "Partner2", 111L, "05"));
        transactions.add(new IndexedTransaction("Transaction11", now.plusDays(1), "Partner3", 111L, "1"));
        transactions.add(new IndexedTransaction("Transaction2", now.plusDays(10), "Partner1", 111L, "4"));
        transactions.add(new IndexedTransaction("Transaction9", now.plusDays(5), "Partner3", 111L, "3"));
        transactions.add(new IndexedTransaction("Transaction7", now.plusDays(1), "Partner2", 111L, "01"));
        transactions.add(new IndexedTransaction("Transaction10", now.plusDays(10), "Partner3", 111L, "4"));
        transactions.add(new IndexedTransaction("Transaction18", now.plusDays(99), "Partner2", 111L, "14"));
        transactions.add(new IndexedTransaction("Transaction19", now.plusDays(90), "Partner2", 111L, "13"));
        transactions.add(new IndexedTransaction("Transaction4", now.plusDays(3), "Partner1", 111L, "2"));
        transactions.add(new IndexedTransaction("Transaction12", now.plusDays(3), "Partner3", 111L, "2"));
        transactions.add(new IndexedTransaction("Transaction5", now.plusDays(5), "Partner2", 111L, "03"));
        transactions.add(new IndexedTransaction("Transaction13", now.plusDays(55), "Partner2", 111L, "09"));
        transactions.add(new IndexedTransaction("Transaction15", now.plusDays(35), "Partner2", 111L, "07"));
        transactions.add(new IndexedTransaction("Transaction16", now.plusDays(45), "Partner2", 111L, "08"));
        transactions.add(new IndexedTransaction("Transaction17", now.plusDays(76), "Partner2", 111L, "11"));
        transactions.add(new IndexedTransaction("Transaction23", now.plusDays(88), "Partner2", 111L, "12"));
        transactions.add(new IndexedTransaction("Transaction20", now.plusDays(293), "Partner2", 111L, "15"));
        transactions.add(new IndexedTransaction("Transaction21", now.plusDays(34), "Partner2", 111L, "06"));
        return transactions;
    }

}