package jikra.name.interview.tm.common;

import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

import static jikra.name.interview.tm.common.Strings.fm;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;


/**
 * Test for {@link Strings}.
 */
@DisplayName("Strings")
class StringsTest {

    @Nested
    @DisplayName("calling leftPadWithZero")
    class CallingLeftPadWithZeroes {

        @Test
        @DisplayName("given negative length, expects exception")
        void givenNegativeLength_expectsIllegalArgumentException() {
            assertThatThrownBy(() -> Strings.leftPadWithZeroes(2, -1))
                    .isInstanceOf(IllegalArgumentException.class)
                    .hasMessage("length must be a positive number");
        }

        @Test
        @DisplayName("given zero length, expects exception")
        void givenZeroLength_expectsIllegalArgumentException() {
            assertThatThrownBy(() -> Strings.leftPadWithZeroes(2, 0))
                    .isInstanceOf(IllegalArgumentException.class)
                    .hasMessage("length must be a positive number");
        }

        @Test
        @DisplayName("given negative value, expects exception")
        void givenNegativeValue_expectsIllegalArgumentException() {
            assertThatThrownBy(() -> Strings.leftPadWithZeroes(-2, 5))
                    .isInstanceOf(IllegalArgumentException.class)
                    .hasMessage("value must be a zero or a positive number");
        }

        @Test
        @DisplayName("given zero value and length of 5, expects five zeros")
        void givenZeroValue_returnsLeftPaddedValue() {
            final String result = Strings.leftPadWithZeroes(0, 5);

            assertThat(result)
                    .isEqualTo("00000");
        }

        @Test
        @DisplayName("given positive value of 55 and length of 5, expects 00055")
        void givenPositiveValue_returnsLeftPaddedValue() {
            final String result = Strings.leftPadWithZeroes(55, 5);

            assertThat(result)
                    .isEqualTo("00055");
        }

        @Test
        @DisplayName("given value bigger then length, expects the whole value and no left-padded with zeros")
        void givenPositiveValue_biggerThenLength_returnsValue() {
            final String result = Strings.leftPadWithZeroes(3333333, 5);

            assertThat(result)
                    .isEqualTo("3333333");
        }

        @Test
        @DisplayName("given value and correct number of params returns formatted message")
        void givenStringAndParams_returnsFormattedMessage() {
            final String result = fm("Test {} test {}, test {}", 1, 2, 3);

            assertThat(result)
                    .isEqualTo("Test 1 test 2, test 3");
        }

        @Test
        @DisplayName("given value and more params returns formatted message")
        void givenStringAndMoreParams_returnsFormattedMessage() {
            final String result = fm("Test {} test {}, test {}", 1, 2, 3, 4);

            assertThat(result)
                    .isEqualTo("Test 1 test 2, test 3");
        }

        @Test
        @DisplayName("given value and less params returns formatted message")
        void givenStringAndLessParams_returnsFormattedMessage() {
            final String result = fm("Test {} test {}, test {}", 1, 2);

            assertThat(result)
                    .isEqualTo("Test 1 test 2, test {}");
        }

        @Test
        @DisplayName("given value and no params returns the same message")
        void givenStringAndNoParams_returnsMessage() {
            final String result = fm("Test {} test {}, test {}");

            assertThat(result)
                    .isEqualTo("Test {} test {}, test {}");
        }

        @Test
        @DisplayName("given null value and no params returns null")
        void givenNullAndNoParams_returnsNull() {
            final String result = fm(null);

            assertThat(result)
                    .isEqualTo(null);
        }
    }
}