package jikra.name.interview.tm.service.api;


/**
 * Service to parse transaction.
 */
public interface TransactionParser {

    /**
     * Parses {@link Transaction} from a {@code value}.
     *
     * @param value value to be parsed
     * @return transaction
     */
    Transaction parse(String value);
}
